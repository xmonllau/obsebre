#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
       dl-fotossol.py

       Copyright 08/2014 Xavi Monllau <xmonllau@obsebre.es>
'''
import os
import xmon
import wget
import urllib3
from argparse import ArgumentParser

def Bjpg(url):
	http = urllib3.PoolManager()
	r = http.request('GET', url)
	Chtml = str(r.data)
	links = []
	#jpg = Chtml.index('<option value=', 0)
	jpg_fin=0
	try:
		while jpg_fin != -1:
			jpg_fin = Chtml.index('.jpg', jpg_fin+1)
			jpg_ini = jpg_fin -16
			jpg_link=Chtml[jpg_ini:jpg_fin+4]
			if jpg_link != "":
				links.append(jpg_link)
	except ValueError:
		return links

def main():
	"""Descarga els fitxers de la web de les fotos solars"""
	argp = ArgumentParser(description='Descarga els fitxers de la web de les fotos solars',epilog='Copyright 2014 xmonllau@obsebre.es bajo licencia GPL v3.0')
	argp.add_argument('-i','--ifi',default='http://www.obsebre.es/php/meteosol/j3_fotossol.php?llengua=cat',type=str,help='url web.')
	argp.add_argument('-d','--odi',default=os.getenv("HOME")+'/FotoSol.www',type=str,help='Directorio de trabajo.')
	argumento = argp.parse_args()

	jpg_links=Bjpg(argumento.ifi)
	if not os.path.exists(argumento.odi):
		os.makedirs(argumento.odi)
	for l in jpg_links:
		if l[0] in 'Xx':
			jpg_O=l
			print (jpg_O)
			wg = ("-c --user-agent= -a /tmp/FotosSol.log \'http://www.obsebre.es/images/oeb/fotossol/"+l+"\' -O \'"+argumento.odi+"/"+jpg_O+"\'")
			xmon.wget(wg,1)
			#filename = wget.download('http://www.obsebre.es/images/oeb/fotossol/'+l)
	return 0

if __name__ == '__main__':
	xmon.clear()
	main()