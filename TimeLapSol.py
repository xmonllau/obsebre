#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  timelapse_fotosol.py
#
#  Copyright 01/2015 xavi monllau <xavi@monllau.es>
#
import os
import shutil
import xmon
from argparse import ArgumentParser

def main():
	"""Crea video TimeLapse de les fotos solars"""
	argp = ArgumentParser(description='Crea video TimeLapse de les fotos solars',epilog='Copyright 2015 xmonllau@obsebre.es bajo licencia GPL v3.0')
	argp.add_argument('-i','--idi',default=os.getenv("HOME")+'/FotoSol.www',type=str,help='Directorio de las fotografias.')
	argp.add_argument('-a','--any',default='16',type=str,help='Any del video.')
	argumento = argp.parse_args()

	#creamos el fotograma base con los logos.
	xmon.clear()
	tmp0=xmon.temp("base_")
	logo_obs = os.getenv("HOME")+"/bin/img/logo_obs.png"
	#logo_obs = os.getenv("HOME")+"/bin/img/logo_obs_url.png"
	logos = " -pointsize 36 -draw 'gravity SouthEast fill white  text 20,20 \"www.obsebre.es\" gravity NorthWest image Over 20,20 460,237 \""+logo_obs+"\"' "
	Base=tmp0+"PngBase.png"
	f_Base="convert -size 1920x1080 xc:black"+logos+Base
	xmon.run (f_Base,1)
	#creamos lista de fotogramas.
	files=sorted(xmon.BuscarFicheros(argumento.idi,extensions=['.jpg','.JPG'],rec=True))
	tmp=xmon.temp("jpg_")
	for FileSol in files:
		if xmon.FicheroExt(FileSol) in ".jpg.JPG":
			if 'x'+argumento.any in xmon.FicheroBase(FileSol):
				#Recortamos imagenes del sol.
				#R_convert = "convert -trim -fuzz 25% "+FileSol+" "+tmp+xmon.FicheroBase(FileSol)+".jpg"
				R_convert = "convert -fill '#000000' -fuzz 55% -floodfill +1+1 '#000000' -fuzz 10% -trim "+FileSol+" "+tmp+xmon.FicheroBase(FileSol)+".jpg"
				xmon.run (R_convert,1)
				#Creamos los fotogramas del sol, fotograma base + imagen recortada.
				R_composite = "composite -gravity Center "+tmp+xmon.FicheroBase(FileSol)+".jpg"+" "+Base+" "+tmp+xmon.FicheroBase(FileSol)+".jpg"
				xmon.run (R_composite,1)
				#Añadimos texto con la fecha de la imagen
				fecha = xmon.FicheroBase(FileSol)[5:7]+"/"+xmon.FicheroBase(FileSol)[3:5]+"/"+xmon.FicheroBase(FileSol)[1:3]
				R_convert = "convert -pointsize 36 -draw 'gravity SouthWest fill white text 20,20 \""+fecha+"\"' "+tmp+xmon.FicheroBase(FileSol)+".jpg"+" "+tmp+xmon.FicheroBase(FileSol)+".jpg"
				xmon.run (R_convert,1)
	#creamos el video
	R_TimeLapSol = "TimeLapse.py --fps 5 --hd hd1080 --vext mp4 --idi "+tmp+" --odi /tmp/VideoSol/ --ofi VideoSol_"+argumento.any
	xmon.run (R_TimeLapSol,1)
	#borramos los directorios temporales
	shutil.rmtree(tmp0,1)
	shutil.rmtree(tmp,1)
	return 0
if __name__ == '__main__':
	main()