#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  CopyFotoSol.py
#
#  Copyright 2013 xmonllau <xmonllau@pcxmonllau-HP>
#
import xmon
import shutil

xmon.clear()
def main():
	bmp = True
	jpg = False
	fit = False
	cp_i="/home/xavi/FotoSol/"
	cp_o="/tmp/FotoSol/"
	xmon.crear_dir(cp_o)
	fotos = sorted(xmon.FicherosEnDir(cp_i))
	for fsol in fotos:
		if xmon.FicheroExt(fsol) == ".bmp" or xmon.FicheroExt(fsol) == ".BMP":
			if xmon.FicheroBase(fsol)[0] == "x":
				fsol_o = fsol.replace (cp_i,cp_o)
				#fsol_o = fsol_o.replace ("BMP","bmp")
				fsol_o_path = xmon.FicheroPath(fsol_o)
				xmon.crear_dir(fsol_o_path)
				if bmp:
					print ("copiando: "+fsol+" > "+fsol_o)
					shutil.copyfile(fsol,fsol_o)
				if jpg:
					Rjpg = "convert "+fsol+" "+fsol_o.replace ("bmp","jpg")
					xmon.run(Rjpg,1)
				if fit:
					Rfit = "convert "+fsol+" "+fsol_o.replace ("bmp","fit")
					xmon.run(Rfit,1)
	return 0
if __name__ == '__main__':
	main()