#! /usr/bin/env python3
"""
Created on Mon Dec 16 15:34:25 2013

@author: xmonllau@obsebre.es
"""
import sys
import os
from PIL import Image

#colores
gris=(127,127,127)
verde=(0,255,0)
cyan=(0,255,255)
rojo=(255,0,0)
azul=(0,0,255)
magenta=(255,0,255)
amarillo=(255,255,0)

DirDcmp=os.getenv("HOME")+"/curves/D/CUR/"
for cur in range (1,4):
	# leemos el fichero scaneado (.tif)
	Ftif=DirDcmp+"S"+sys.argv[1]+str(cur)+".TIF"
	if os.path.isfile(Ftif) == True:
		im=Image.open(Ftif).convert("RGB")
		xsize, ysize = im.size # ancho y alto del fichero
		print(Ftif,xsize,ysize)
		if xsize != 1272 or ysize != 1650:
			print ("el fichero tif no tiene las dimensiones correctas")
		# dibujamos las marcas de hora (.hor)
		Dhor=[]
		Fhor=Ftif.replace(".TIF",".HOR")
		if os.path.isfile(Fhor) == True:
			hor = open(Fhor, 'r')
			for line in hor:
				Dhor.append(line.split())
			hor.close()
			for line in range(1,len(Dhor)-2):
				for mhor in range (10,414):
					im.putpixel((mhor,int(Dhor[line][0])),gris)
				for mhor in range (435,838):
					im.putpixel((mhor,int(Dhor[line][1])),gris)
				for mhor in range (858,1262):
					if int(Dhor[line][2]) > 0:
						im.putpixel((mhor,int(Dhor[line][2])),gris)
		else:
			print ("No encuentro"+Fhor)
		# dibujamos las curvas y ejes (.tbl)
		Dtbl=[]
		Ftbl=Ftif.replace(".TIF",".TBL")
		if os.path.isfile(Ftbl) == True:
			tbl = open(Ftbl, 'r')
			for line in tbl:
				Dtbl.append(line.split())
			tbl.close()
			for line in range(0,len(Dtbl)):
				for col in range(1,7):
					if col == 1 or col == 2:
						color = verde
					if col == 3 or col == 4:
						color = cyan
					if col == 5 or col == 6:
						color = rojo
					if Dtbl[line][col] != "0":
						if int(Dtbl[line][col]) < 1271:
							im.putpixel((int(Dtbl[line][col]),int(Dtbl[line][0])),color)
						else:
							print (cur,Dtbl[line][0],Dtbl[line][col],"pixel fuera del scanner")
		else:
			print (Ftbl+" No existe")
		# dibujamos los puntos iniciales (.mar)
		Dmar=[]
		Fmar=Ftif.replace(".TIF",".MAR")
		if os.path.isfile(Fmar) == True:
			mar = open(Fmar, 'r')
			for line in mar:
				Dmar.append(line.split())
			mar.close()
			for line in range(0,len(Dmar)-1):
				im.putpixel((int(Dmar[line][0]),int(Dmar[line][1])),azul)
		else:
			print (Fmar+" No existe")
		# grabamos el fichero
		ofi=DirDcmp+"S"+sys.argv[1]+str(cur)+".PNG"
		im.save(ofi,quality=100)
		#im.show()
	else:
		print (Ftif+" No existe")
		Fpng=DirDcmp+"S"+sys.argv[1]+str(cur)+".PNG"
		N_tif="convert -size 1272x1650 xc:white "+Fpng
		#print (N_tif)
		os.system(N_tif)
#generamos el fichero con los tres trozos
Fmontage=""
for cur in range (1,4):
	# leemos el fichero scaneado (.tif)
	Fpng=DirDcmp+"S"+sys.argv[1]+str(cur)+".PNG"
	if os.path.isfile(Fpng) == True:
		Fmontage = Fmontage+" "+Fpng
	else:
		print ("No encuentro"+Fpng)
Cmontage="montage -rotate 270 -geometry +0+0 -density 150 -units PixelsPerInch -tile 3x1"+Fmontage+" "+DirDcmp+"S"+sys.argv[1]+".PNG"
#print (Cmontage)
os.system(Cmontage)