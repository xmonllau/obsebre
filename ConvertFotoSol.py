#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  ConvertFotoSol.py
#
#  Copyright 2013 xmonllau <xmonllau@pcxmonllau-HP>
#
import sys
import os
import xmon

def main():
	tmp="/tmp/FotoSol/"
	if not os.path.exists(tmp):
		os.makedirs(tmp)
	fotos = sorted(xmon.FicherosEnDir(sys.argv[1]))
	ext = sys.argv[2]
	for fsol in fotos:
		bitmap_in=fsol
		bitmap_out=tmp+xmon.FicheroBase(fsol)+"."+ext
		crear_bitmap="convert "+bitmap_in+" -flip "+bitmap_out
		xmon.run(crear_bitmap, 1)
	return 0
if __name__ == '__main__':
	main()
