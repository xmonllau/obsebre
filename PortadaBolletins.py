#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  PortadaBolletins.py
#
#  Copyright 03/2015 xmonllau <xmonllau@obsebre.es>
#
import os
import sys
import xmon
import shutil
from configparser import SafeConfigParser
from argparse import ArgumentParser


xmon.clear()

def main():
	#Valores por defecto desde un fichero
	app_cfg = os.getenv("HOME")+"/.xmon"
	cfg = app_cfg+"/PortadaBolletins.cfg"
	if not os.path.exists(cfg):
		print ('Me falta el fichero de configuracion: '+cfg)
		sys.exit(0)
	else:
		cparser = SafeConfigParser()
		cparser.read(cfg)

	xmon.clear()
	argp = ArgumentParser(description='Añade las portadas al boletin', epilog='Copyright 03/2015 xmonllau@obsebre.es')
	argp.add_argument('-v','--version', action='version', version='%(prog)s 1.0')
	argp.add_argument('-p','--plan', default=cparser.get('PorDefecto','plan'), type=str, help='Directori de les plantillas')
	argp.add_argument('-a','--año', default=cparser.get('PorDefecto','año'), type=str, help='Any del butlleti')
	argp.add_argument('-s','--seccion', default=cparser.get('PorDefecto','seccion'), type=str,choices=['ebre','livingston','ionosfera','meteorologia'], help='Seccio del butlleti')
	argp.add_argument('-d','--dpi', default=cparser.get('PorDefecto','dpi'), type=str, help='dpi\'s del butlleti')
	argumento = argp.parse_args()

	#reemplazamos las variables de la plantilla svg.
	p_svg=os.getenv("HOME")+argumento.plan+'/'+argumento.seccion+'.svg'
	tmp = xmon.temp("buttlletins_")
	svg_m=tmp+argumento.seccion+'_'+argumento.año+'.svg'
	svg_var={"_XX_":argumento.año,"_XX_+1":argumento.año}
	#leemos el svg original
	ifi = open(p_svg, 'r')
	infile = ifi.read()
	ifi.close()
	#modificamos las variables en la plantilla
	svg_nuevo=xmon.SVGreplace(infile,svg_var)
	#guardamos el svg modificado
	ofi = open(svg_m, 'w')
	ofi.write(svg_nuevo)
	ofi.close()
	#convertimos el svg en png
	svg = sorted(xmon.FicherosEnDir(tmp))
		xmon.svg2bitmap(svg,tmp,'d','png',0,0)
	if argumento.dpi == '300':
		#convertimos el png en portada y contraportada
		ConvertJPG = 'ConvertBitmap.py -i '+tmp+argumento.seccion+'_'+argumento.año+'.png -f jpg -c 2480x3508 --odi '+tmp
		xmon.run(ConvertJPG,1)
		ConvertPDF0 = 'ConvertBitmap.py -i '+tmp+argumento.seccion+'_'+argumento.año+'-0.jpg -f pdf -c 2480x3508 --odi '+tmp
		xmon.run(ConvertPDF0,1)
		ConvertPDF1 = 'ConvertBitmap.py -i '+tmp+argumento.seccion+'_'+argumento.año+'-1.jpg -f pdf -c 2480x3508 --odi '+tmp
		xmon.run(ConvertPDF1,1)
		#creamos una hoja en blanco
		p_blanco='convert -size 2480x3508 xc:white -density '+argumento.dpi+' -units PixelsPerInch '+tmp+'hb.pdf'
		xmon.run (p_blanco,1)
		#creamos el pdf final
		pdftk = "pdftk "+tmp+argumento.seccion+'_'+argumento.año+'-1.pdf '+tmp+'hb.pdf _'+argumento.seccion+'_'+argumento.año+'.pdf '+tmp+'hb.pdf '+tmp+argumento.seccion+'_'+argumento.año+'-0.pdf '+"cat output "+argumento.seccion+'_'+argumento.año+'.pdf'
		xmon.run(pdftk,1)
	#Borramos directorio temporal
	shutil.rmtree(tmp)
	return 0


if __name__ == '__main__':
	main()