#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 27 23:16:55 2014

@author: xavi@monllau.es
"""

import urllib, os, xmon, time, Gnuplot, Gnuplot.funcutils,shutil
from argparse import ArgumentParser
global dades,minut
def main():
	global dades,minut
	dades = []; minut = []
	inicio = time.time() #tiempo inicial

	argp = ArgumentParser(description='Genera video de la bruixola',epilog='Copyright 04/2014 xavi@monllau.es')
	argp.add_argument('-u','--url',default='',type=str,help='fecha del video, (mmmddaa)')
	argp.add_argument('-o','--obs',default='HRT',type=str,choices=['EBR','HRT','LIV'],help='Observatorio (EBR, HRT, LIV)')
	argp.add_argument('-i','--ifi',default='',type=str,help='fecha del video, (mmmddaa)')
	argp.add_argument('--hd',default='hd720',type=str,choices=['hd480','hd720'],help='resolucion del video (hd480, hd720)')
	argp.add_argument('-s','--sleep',default='0',type=float,help='pausa al plotear')
	argumento = argp.parse_args()

	if argumento.url == "":
		if argumento.obs == "EBR":
			l_obs="EBR"
		elif argumento.obs == "HRT":
			l_obs="EBR"
			Fhrt="http://www.obsebre.es/php/geomagnetisme/rthorta/20"+argumento.ifi[5:7]+"/"+argumento.ifi[:3]+"/"+argumento.ifi+".HRT"
		elif argumento.obs == "LIV":
			l_obs="LIV"
			Fhrt="http://www.obsebre.es/php/geomagnetisme/rtliving/20"+argumento.ifi[5:7]+"/"+argumento.ifi[:3]+"/"+argumento.ifi+".LIV"
	else:
		Fhrt=argumento.url+"/20"+argumento.ifi[5:7]+"/"+argumento.ifi[:3]+"/"+argumento.ifi

	try:	
		f = urllib.urlopen(Fhrt)
	except :
		print ("error al bajar el fichero...")
		pass
	else:
		for line in f:
			linea = line.split()
			if linea[0] == l_obs:
				data=linea[1]
				hora=linea[3]
				min=0
			else:
				for v in [1,5]:
					G = 0.0137*float(linea[v]) + 1.9
					entero = int(G)
					decimal = abs(G) - abs(int(G))
					if decimal < .26:
							R=entero
					elif decimal > .25 and decimal < .76:
						if G > 0:
							R=entero+.5
						else:
							R=entero-.5
					elif decimal > .75 and decimal < 1:
						if G > 0:
							R=entero+1
						else:
							R=entero-1
					video="/home/xavi/bin/img/compass/compass_"+str(R).replace(".","")+".png"
					if not os.path.exists(video):
						video=""
					video="/home/xavi/bin/img/compass/compass_"+str(R).replace(".","")+".png"
					video="/home/xavi/bin/img/compass/compass_"+str(R)+".png"
					minut = [data,hora.zfill(2),str(min).zfill(2),linea[v],G,R,video]
					dades.append(minut)
					minut.remove
					min+=1
		dades2 = [i[4:5] for i in dades]
		compass_dir="/tmp/compass/"
		
		logo_obs = "/home/xavi/bin/img/logo_obs.png"
		#creamos el fotograma base con los logos.
		tmp=xmon.temp(".")
		logos = " -pointsize 36 -draw 'gravity SouthEast fill black  text 20,20 \"www.obsebre.es\" gravity NorthWest image Over 20,20 460,237 \""+logo_obs+"\"' "
		Base=tmp+"PngBase.png"
		f_Base="convert -size 1280x720 xc:white"+logos+Base
		xmon.run (f_Base,0)		
		
		if not os.path.exists(compass_dir):
			os.makedirs(compass_dir)

		Gmin=1				
		for minuto in dades:
			if minuto[6] != "":
				#creamos la grafica
				grf_src=tmp+"g"+minuto[1]+minuto[2]+".png"
				gplot=Gnuplot.Gnuplot(debug=0)
				gplot('set term png size 1250,380')
				gplot('set style data lines')
				gplot('set xrange [0:1440]')
				gplot('set xtics ("00" 0,"01" 60,"02" 120,"03" 180,"04" 240,"05" 300,"06" 360,"07" 420,"08" 480,"09" 540,"10" 600,"11" 660,"12" 720,"13" 780,"14" 840,"15" 900,"16" 960,"17" 1020,"18" 1080,"19" 1140,"20" 1200,"21" 1260,"22" 1320,"23" 1380)')
				gplot('set yrange [-15:15]')
				gplot('set ytics ("-15\'" -15, "-10\'" -10, "-5\'" -5, "0\'" 0, "5\'" 5, "10\'" 10, "15\'" 15)')
				png="set output \""+grf_src+"\""
				gplot(png)
				gplot.plot(dades2[:Gmin])
				Gmin+=1
				if argumento.sleep > 0:
					time.sleep(argumento.sleep)

				src=minuto[6]
				#Creamos fotograma+compass.
				dst1=tmp+"c"+minuto[1]+minuto[2]+".png"
				R_composite = "composite -geometry +700+60 "+src+" "+Base+" "+dst1
				xmon.run (R_composite,0)
				#Creamos fotograma+grf.				
				dst2=tmp+"f"+minuto[1]+minuto[2]+".png"				
				R_composite2 = "composite -geometry +16+260 "+grf_src+" "+dst1+" "+dst2
				xmon.run (R_composite2,0)
				#Añadimos fecha y hora
				dst3=compass_dir+"f"+minuto[1]+minuto[2]+".png"				
				R_convert = "convert -pointsize 32 -draw 'gravity SouthWest fill black text 20,20 \""+minuto[0]+" - "+minuto[1]+":"+minuto[2]+"\"' "+dst2+" "+dst3
				xmon.run (R_convert,1)
		#creamos el video
		R_TimeLapCompass = "TimeLapse.py --fps 60 --hd "+argumento.hd+" --vext mp4 --idi /tmp/compass --odi /tmp/VideoCompass/ --ofi "+minuto[0]+"."+argumento.obs
		xmon.run (R_TimeLapCompass,0)
		shutil.rmtree(tmp, 1)
		shutil.rmtree(compass_dir, 1)
		fin = time.time() #tiempo final
		print("Tiempo total de ejecucion =",int((fin - inicio)/60), "minutos") #tiempo de ejecucion

if __name__ == '__main__':
	main()